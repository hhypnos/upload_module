import { Injectable } from '@nestjs/common';
import * as multer from 'multer';
import * as AWS from 'aws-sdk';
import * as multerS3 from 'multer-s3';
import {Persons} from '../../entities/persons.entity';
import {PersonImages} from '../../entities/personImages.entity';
import {Images} from '../../entities/images.entity';
import {UploadNodes} from '../../entities/uploadNodes.entity';
import {Uploads} from '../../entities/uploads.entity';
import {Comments} from '../../entities/comments.entity';
import {getToday, uploadDocument} from '../helper';

const s3 = new AWS.S3();
AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});
@Injectable()
export class UploadService {

    cv = multer({
        fileFilter(req, file, cb) {
            const getFileType = (file.originalname.split('.').pop(-1)).toLowerCase();
            if (getFileType !== 'pdf' && getFileType !== 'docx' && getFileType !== 'doc') {
                return cb(new Error('Only pdfs, docx and doc are allowed'));
            }
            cb(null, true);
        },
        storage: multerS3({
            s3,
            bucket: process.env.AWS_S3_BUCKET_NAME,
            acl: 'public-read',
            key: (request, file, cb) => {
                cb(null, `documents/${request.person_id}/${Date.now().toString()} - ${file.originalname}`);
            },
        }),
        limits : {
            fileSize : 15728640,
        },
    }).array('cv');

    async fileUpload(req, res, data) {
        try {
            req.person_id = data.user;
            this.cv(req, res, async (error) => {
                if (error) {
                    console.log(error);
                    return res.status(404).json(`Failed to upload file: ${error}`);
                }
                const uploadNodes = await uploadDocument(data.user, req.files[0].key, req.files[0].originalname,
                    req.files[0].encoding, req.files[0].size, req.files[0].metadata, req.files[0].key,
                    req.files[0].location, data.relation_table, data.type_id, data.dataId);
                return res.status(200).json({status: 'success', result: {data: uploadNodes}});
            });
        } catch (error) {
            console.log(error);
            return res.status(500).json(`Failed to upload file: ${error}`);
        }
    }

    image = multer({
        fileFilter(req, file, cb) {
            const getFileType = (file.originalname.split('.').pop(-1)).toLowerCase();
            if (getFileType !== 'png' && getFileType !== 'jpg' && getFileType !== 'jpeg') {
                return cb(new Error('Only png, jpg and jpeg are allowed'));
            }
            cb(null, true);
        },
        storage: multerS3({
            s3,
            bucket: process.env.AWS_S3_BUCKET_NAME,
            acl: 'public-read',
            key: (request, file, cb) => {
                cb(null, `images/${request.person_id}/${Date.now().toString()} - ${file.originalname}`);
            },
        }),
        limits : {
            fileSize : 15728640,
        },
    }).array('image');

    async imageUpload(req, res, data) {
        try {
            req.person_id = data.user;
            this.image(req, res, async (error) => {
                if (error) {
                    console.log(error);
                    return res.status(404).json(`Failed to upload file: ${error}`);
                }

                const uploadNodes = await uploadDocument(data.user, req.files[0].key, req.files[0].originalname,
                    req.files[0].encoding, req.files[0].size, req.files[0].metadata, req.files[0].key,
                    req.files[0].location, data.relation_table, data.type_id, data.dataId);
                return res.status(200).json({status: 'success', result: {data: uploadNodes}});
            });
        } catch (error) {
            console.log(error);
            return res.status(500).json(`Failed to upload file: ${error}`);
        }
    }

    async deleteDocument(id: number) {
        const deleteDocument = await UploadNodes.findOne({  where: {
                id,
            },
        });
        deleteDocument.deleted_at = getToday();
        return UploadNodes.save(deleteDocument);
    }

    async fetchDocument(id: number, relationTable: string) {
        const fetchDocument = await UploadNodes.findAndCount({  where: {
                data_id: id,
                relation_table: relationTable,
            },
            relations: ['file_id'],
        });
        return fetchDocument;
    }
}
