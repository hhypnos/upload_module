import {
    Controller,
    Get,
    Post,
    Param,
    Res,
    Req,
    HttpStatus,
    HttpCode,
    Body,
    HttpService,
    Query,
    UseGuards,
} from '@nestjs/common';
import { Request, Response } from 'express';

import {UploadService} from '../upload/upload.service';
import {JwtAuthGuard} from '../auth/jwt-auth.guard';
import {commentLog} from '../helper';

@Controller('upload')
export class UploadController {
    constructor(private readonly uploadService: UploadService) {}

    @UseGuards(JwtAuthGuard)
    @Post('delete/:id')
    async deleteDocument(@Param() params, @Req() req, @Res() res: Response) {
        const deleteDocument = await this.uploadService.deleteDocument(params.id);
        await commentLog(`Document was deleted`, 'upload_nodes', req.user.person.id, deleteDocument.file_id.uploaded_by, null);
        return res.status(200).json({status: 'success', result: {data: deleteDocument}});
    }
}
