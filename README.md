<h1>Description</h1>

* Sync entity in database
* ![alt text](database.png)
* type_id is abstract in my project type_id: 1 is used for photos, type_id: 2 is used for documents
* relation_table is which table it belongs to data_id

<h1>Example</h1>
 
```
constructor(private readonly uploadService: UploadService) {}

@Post('upload/image/:id')
async uploadImage(@Param() params: CreatePersonDtoParams, @Body() body, @Req() req, @Res() res: Response) {
    return this.uploadService.imageUpload(req, res, {
        dataId: params.id, 
        user: req.user.person.id, 
        type_id:1, 
        relation_table:'person'
    });
}
```