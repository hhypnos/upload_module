import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    OneToOne,
    OneToMany,
    JoinTable,
    JoinColumn,
    BaseEntity,
    ManyToOne,
    ManyToMany,
} from 'typeorm';
import {Positions} from './positions.entity';
import {Persons} from './persons.entity';
import {SourceTypes} from './sourceTypes.entity';

@Entity()
export class Uploads extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons, person => person.id, {nullable: true})
    @JoinColumn({name: 'uploaded_by'})
    uploaded_by: number;

    @Column({type: 'varchar', length: 255})
    name: string;

    @Column({type: 'varchar', length: 255})
    name_original: string;

    @Column({type: 'varchar', length: 255, nullable: true})
    encoding: string;

    @Column({type: 'int', nullable: true})
    size: number;

    @Column({type: 'varchar', length: 255, nullable: true})
    metadata: string;

    @Column({type: 'varchar', length: 255, nullable: true})
    path: string;

    @Column({type: 'varchar', length: 255, nullable: true})
    full_path: string;

    @ManyToOne(type => SourceTypes, sourceType => sourceType.id, {nullable: true})
    @JoinColumn({name: 'source_id'})
    source_id: number;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

    @Column({ type: 'timestamp', nullable: true, default: null })
    deleted_at: string;

}
