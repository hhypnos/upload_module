import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    OneToOne,
    OneToMany,
    JoinTable,
    JoinColumn,
    BaseEntity,
    ManyToOne,
    ManyToMany,
} from 'typeorm';
import {Positions} from './positions.entity';
import {Persons} from './persons.entity';
import {Uploads} from './uploads.entity';
import {RelationTables} from './relationTables.entity';

@Entity()
export class UploadNodes extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Uploads, upload => upload.id, {nullable: true})
    @JoinColumn({name: 'file_id'})
    file_id: Uploads;

    @Column({type: 'int', nullable: true})
    type_id: number;

    @Column({type: 'int', nullable: true})
    data_id: number;

    @Column({type: 'varchar', length: 191, nullable: true})
    relation_table: string;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

    @Column({ type: 'timestamp', nullable: true, default: null })
    deleted_at: string;

}
